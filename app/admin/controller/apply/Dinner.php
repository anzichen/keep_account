<?php

namespace app\admin\controller\apply;

use app\common\model\CostBill;
use app\common\model\DinnerRecord;
use Exception;
use ba\Random;
use think\facade\Db;
use app\common\controller\Backend;
use app\admin\model\User as UserModel;
use app\admin\model\UserGroup;
use think\db\exception\PDOException;
use think\exception\ValidateException;

class Dinner extends Backend
{
    protected $model = null;

    protected $withJoinTable = ['user'];


    protected $quickSearchField = ['summary', 'amount', 'id'];

    public function initialize()
    {
        parent::initialize();
        $this->model = new DinnerRecord();
    }


    public function edit($id = null)
    {
        $row = $this->model->find($id);
        if (!$row) {
            $this->error(__('Record not found'));
        }

        if ($this->request->isPost()) {
        

            parent::edit($id);
        }



        $this->success('', [
            'row' => $row
        ]);
    }
}