<?php

namespace app\common\model;

use think\Model;
use app\admin\model\Admin;
use app\admin\library\Auth;
use app\admin\model\MenuRule;
use think\model\concern\SoftDelete;
use app\admin\model\User;
/**
 * AdminLog模型
 * @controllerUrl 'authAdminLog'
 */
class DinnerRecord extends Model
{
    use SoftDelete;
    protected $autoWriteTimestamp = 'int';

    protected $createTime = 'create_time';
    protected $updateTime ='update_time';
    public function user(){
        return $this->belongsTo('app\admin\model\User','user_id');
    }
}