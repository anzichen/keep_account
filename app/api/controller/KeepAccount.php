<?php

namespace app\api\controller;

use app\common\model\CostBill;
use app\common\controller\Api;
use app\common\model\DinnerRecord;

class KeepAccount extends Api
{
    protected $noNeedLogin = ['oauth_login','refresh_token'];
    protected $noNeedPermission= ['*'];
    public function initialize()
    {
        parent::initialize();
    }
    public function get_book_list()
    {

        $days = array_reverse(get_day(date('Y-m-d'),2));
        $total_cost=CostBill::whereBetween('create_time',[strtotime(date('Y-m')),time()])->sum('amount');
        $my_cost=CostBill::whereBetween('create_time',[strtotime(date('Y-m')),time()])->where(['user_id'=>$this->auth->id])->sum('amount');
        $total_times=DinnerRecord::whereBetween('create_time',[strtotime(date('Y-m')),time()])->count('id');
        $my_times=DinnerRecord::whereBetween('create_time',[strtotime(date('Y-m')),time()])->where(['user_id'=>$this->auth->id])->count('id');
        $percent_cost=$my_times>0?bcdiv($total_cost,$total_times,2):0;
        $my_spend= bcmul($percent_cost,$my_times,2);
        foreach ($days as $key=>$value){
            $bill_list[$key]['date']=$value;
            $bill_list[$key]['cost']=CostBill::whereBetween('create_time',[strtotime($value),strtotime($value)+24*3600])->sum('amount');
            $bill_list[$key]['list']=CostBill::with(['user'])->whereBetween('create_time',[strtotime($value),strtotime($value)+24*3600])->order('id','desc')->select();
        }

        $this->success('成功', ['my_cost' =>$my_cost,'percent_cost' =>$percent_cost, 'total_cost' =>$total_cost, 'my_spend' =>$my_spend, 'bill_list' => $bill_list]);
    }
}