<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\CostBill;

class Bill extends Api
{
    protected $noNeedLogin = ['oauth_login', 'refresh_token'];
    protected $noNeedPermission = ['*'];

    public function initialize()
    {

        parent::initialize();
    }


    public function submit_bill()
    {



        if (\app\admin\model\User::where(['id' => $this->auth->id])->value('group_id') != 1) {
            $this->error('暂不支持外部人员使用');
        }
        if (!$this->request->post('imgList')) {
            $this->error('请至少上传一张照片用作存档');
        }
        if (!$this->request->post('amount')) {
            $this->error('金额不能为空');
        }
        if (!$this->request->post('summary')) {
            $this->error('采购商品不能为空');
        }
        CostBill::create([
            'amount' => $this->request->post('amount'),
            'type' => $this->request->post('type'),
            'summary' => $this->request->post('summary'),
            'photos' => json_encode($this->request->post('imgList')),
            'date' => $this->request->post('date'),
            'user_id' => $this->auth->id
        ]);
        $this->success('成功');
    }
    public function get_bill_detail()
    {
        $data=CostBill::with(['user'])->where('id',$this->request->get('id'))->find();
        $data['photo_arr']=json_decode($data['photos'],true);

        $data['is_me']= $data['user_id']==$this->auth->id?1:0;
        $this->success('成功',$data);
    }
    public function delete_bill()
    {
        if (\app\admin\model\User::where(['id' => $this->auth->id])->value('group_id') != 1) {
            $this->error('暂不支持外部人员使用');
        }
        if (!$this->request->post('id')) {
            $this->error('id不能为空');
        }

        if (CostBill::where(['id' => $this->request->post('id')])->value('user_id') != $this->auth->id) {
            $this->error('只能删除自己的报账单');
        }
        // 软删除
        CostBill::destroy($this->request->post('id'));

        $this->success('成功');
    }
}