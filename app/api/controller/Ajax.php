<?php

namespace app\api\controller;

use think\Exception;
use think\exception\FileException;
use app\common\library\Upload;
use app\common\controller\Api;
use think\facade\Db;
use EasyWeChat\Factory;
class Ajax extends Api
{
    protected $noNeedLogin = ['upload','send_subscribe_msg'];
    public function initialize()
    {
        parent::initialize();
    }

    public function upload()
    {
        $file = $this->request->file('file');
        try {
            $upload                = new Upload($file);
            $attachment            = $upload->upload(null, 0, 0);
            $attachment['fullurl'] = full_url($attachment['url']);
            unset($attachment['createtime'], $attachment['quote']);
        } catch (Exception | FileException $e) {
            $this->error($e->getMessage());
        }

        $this->success(__('File uploaded successfully'), [
            'file' => $attachment
        ]);
    }

    public function area()
    {
        $this->success('', get_area());
    }

    public function buildSuffixSvg()
    {
        $suffix     = $this->request->param('suffix', 'file');
        $background = $this->request->param('background');
        $content    = build_suffix_svg((string)$suffix, (string)$background);
        return response($content, 200, ['Content-Length' => strlen($content)])->contentType('image/svg+xml');
    }

    public function send_subscribe_msg(){
        $text='提醒您及时订餐，以免耽误用餐时机';
        $config = [
            'app_id' => 'wx56c979d82ef6e40d',
            'secret' => '71b08c91f4f06acfa635249ae9ffb9f9',

            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
                'file' => __DIR__.'/wechat.log',
            ],
        ];
        $type_text=$this->request->get('type')==1?'今日中餐':'今日晚餐';
        //avatarUrl: "https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132"
        //city: ""
        //country: ""
        //gender: 0
        //language: ""
        //nickName: "微信用户"
        //province: ""
        $app = Factory::miniProgram($config);
        $user_list=\app\admin\model\User::select();
        foreach ($user_list as $key=>$value){
            echo $value['nickname'];
            $data = [
                'template_id' => 'PoqanOeFN4jqEbpPIV-HoGmDmUz5MrpkB3JF1R5_dko', // 所需下发的订阅模板id
                'touser' => $value['openid'],     // 接收者（用户）的 openid
                'page' => 'pages/addmbook/addmbook',       // 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
                'data' => [         // 模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
                    'thing1' => [
                        'value' => $value['nickname'],
                    ],
                    'thing2' => [
                        'value' => $type_text,
                    ],
                    'time3' => [
                        'value' => date('Y-m-d'),
                    ],
                    'thing4' => [
                        'value' =>$text,
                    ],
                ],
            ];

          $r=  $app->subscribe_message->send($data);
          print_r($r);
        }
    }
}