<?php

namespace app\api\controller;

use app\common\model\CostBill;
use app\common\controller\Api;
use app\common\model\DinnerRecord;

class Index extends Api
{
    protected $noNeedLogin = ['oauth_login','refresh_token'];
    protected $noNeedPermission= ['*'];
    public function initialize()
    {
        parent::initialize();
    }
    public function get_book_list()
    {


if($this->request->post('YearMonth')){
    $date=str_replace('月','',str_replace('年','-',$this->request->post('YearMonth'))).'-01';
}else{
    $date=date('Y-m-d');
}
//echo $date;
        $firstday = date('Y-m-01', strtotime($date));
        $lastday = date('Y-m-d', strtotime("$firstday +1 month"));

        $days = array_reverse(get_day($date,2));
        $total_cost=CostBill::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->sum('amount');
       // echo (new CostBill())->getLastSql();
        $dinner_total_cost=CostBill::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->where(['type'=>1])->sum('amount');
        $living_total_cost=CostBill::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->where(['type'=>2])->sum('amount');
        $my_cost=CostBill::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->where(['user_id'=>$this->auth->id])->sum('amount');
        $total_times=DinnerRecord::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->count('id');
        $my_times=DinnerRecord::whereBetween('create_time',[strtotime($firstday),strtotime($lastday)])->where(['user_id'=>$this->auth->id])->count('id');
        $dinner_per_cost=$my_times>0?bcdiv($dinner_total_cost,$total_times,2):0;
        $my_dinner_spend= bcmul($dinner_per_cost,$my_times,2);
        $bill_list=[];
        $dinner_list=[];
        foreach ($days as $key=>$value){
            $bill_list[$key]['date']=$value;
            $dinner_list[$key]['date']=$value;
            $dinner_list[$key]['morning']=DinnerRecord::with(['user'])->where(['date' => $value, 'type' => 1])->select();
            $dinner_list[$key]['lunch']=DinnerRecord::with(['user'])->where(['date' => $value, 'type' => 2])->select();
            $dinner_list[$key]['evening']=DinnerRecord::with(['user'])->where(['date' => $value, 'type' => 3])->select();
            $bill_list[$key]['cost']=CostBill::whereBetween('create_time',[strtotime($value),strtotime($value)+24*3600])->sum('amount');
            $bill_list[$key]['list']=CostBill::with(['user'])->whereBetween('create_time',[strtotime($value),strtotime($value)+24*3600])->order('id','desc')->select();
        }

        $this->success('成功', [
            'total_cost' =>$total_cost,//总开支
            'living_total_cost' =>$living_total_cost,//生活费总支出
            'dinner_total_cost' =>$dinner_total_cost,//餐费总花费
            'dinner_per_cost' =>$dinner_per_cost,//餐费餐均消费
            'my_cost' =>$my_cost,//我的采购垫资
            'my_dinner_spend' =>$my_dinner_spend,//我的餐费应分摊
            'my_living_spend' =>bcdiv($living_total_cost,3,2),//我的物资费应分摊
            'bill_list' => $bill_list,
            'dinner_list' => $dinner_list
        ]);
    }
}