<br />
<div align="center">
    <img src="https://express.anzichen.com.cn/storage/default/20221031/f6735bf0d4e4255a61f9228a82697ea5fbc2df04.png" alt="" />
    <br />
    <a href="https://express.anzichen.com.cn/" target="_blank">演示站</a> |
    <a href="https://express.anzichen.com.cn" target="_blank">文档</a> |
    <a href="https://gitee.com/anzichen/keep_account" target="_blank">Gitee仓库</a> |
   
</div>
<br />
<p align="center">
    <a href="https://www.thinkphp.cn/" target="_blank">
        <img src="https://img.shields.io/badge/ThinkPHP-%3E6.0-brightgreen?color=91aac3&labelColor=439EFD" alt="vue">
    </a>
    <a href="https://v3.vuejs.org/" target="_blank">
        <img src="https://img.shields.io/badge/Vue-%3E3.x-brightgreen?color=91aac3&labelColor=439EFD" alt="vue">
    </a>
    <a href="https://element-plus.gitee.io/#/zh-CN/component/changelog" target="_blank">
        <img src="https://img.shields.io/badge/Element--Plus-%3E2.2-brightgreen?color=91aac3&labelColor=439EFD" alt="element plus">
    </a>
    <a href="https://www.tslang.cn/" target="_blank">
        <img src="https://img.shields.io/badge/TypeScript-%3E4.4-blue?color=91aac3&labelColor=439EFD" alt="typescript">
    </a>
    <a href="https://vitejs.dev/" target="_blank">
        <img src="https://img.shields.io/badge/Vite-%3E2.9-blue?color=91aac3&labelColor=439EFD" alt="vite">
    </a>
    <a href="https://pinia.vuejs.org/" target="_blank">
        <img src="https://img.shields.io/badge/Pinia-%3E2.0-blue?color=91aac3&labelColor=439EFD" alt="vite">
    </a>
    <a href="https://gitee.com/wonderful-code/buildadmin/blob/master/LICENSE" target="_blank">
        <img src="https://img.shields.io/badge/Apache2.0-license-blue?color=91aac3&labelColor=439EFD" alt="license">
    </a>
</p>

<br>

### 项目预览
|  |  |
|---------------------|---------------------|
|![登录](https://wonderful-code.gitee.io/images/readme/login.gif)|![控制台](https://wonderful-code.gitee.io/images/readme/dashboard.png)|
|![布局配置](https://wonderful-code.gitee.io/images/readme/layout.png)|![表格](https://wonderful-code.gitee.io/images/readme/admin.png)|
|![表单](https://wonderful-code.gitee.io/images/readme/user.png)|![系统配置](https://wonderful-code.gitee.io/images/readme/config.png)|
|![数据回收规则](https://wonderful-code.gitee.io/images/readme/data-recycle.png)|![数据回收日志](https://wonderful-code.gitee.io/images/readme/data-recycle-log.png)|
|![敏感数据](https://wonderful-code.gitee.io/images/readme/sensitive-data.png)|![菜单](https://wonderful-code.gitee.io/images/readme/menu.png)|
|![单栏布局](https://wonderful-code.gitee.io/images/readme/layout-3.png)|![经典布局](https://wonderful-code.gitee.io/images/readme/layout-2.png)|



### 特别鸣谢
💕 感谢巨人提供肩膀，排名不分先后
- [Thinkphp](http://www.thinkphp.cn/)
- [FastAdmin](https://gitee.com/karson/fastadmin)
- [BuildAdmin](http://www.buildadmin.com/)
- [Vue](https://github.com/vuejs/core)
- [Uni-app](https://uniapp.dcloud.net.cn/)
- [ColorUi](https://github.com/weilanwl/coloruicss)
### 版权信息
🔐 BuildAdmin 遵循`Apache2.0`开源协议发布，提供无需授权的免费使用。\
本项目包含的第三方源码和二进制文件之版权信息另行标注。

### 支持项目
💕 无需捐赠，如果觉得项目不错，或者已经在使用了，希望你可以去 [Github](https://github.com/build-admin/BuildAdmin) 或者 [Gitee](https://gitee.com/wonderful-code/buildadmin) 帮我们点个 ⭐ Star，这将是对我们极大的鼓励与支持。
