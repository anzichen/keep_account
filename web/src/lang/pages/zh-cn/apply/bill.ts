export default {

'Dinner':'餐费开支',
    'Life':'生活开支',

    'Summary': '商品概览',
    'Date': '采购日期',
    'User': '采购用户',
    'Amount': '采购金额',
    'Photos': '照片存档',
}
